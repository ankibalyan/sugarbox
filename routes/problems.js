var express = require('express');
const { getMinPositiveNum } = require('../utils');
var router = express.Router();

router.get('/p1/:input', function(req, res, next) {
  const { input = 0 } = req.params;
  let output;

  try {
    output = getMinPositiveNum(input);
  } catch (error) {
    console.error(error);
    // return better error
    return next(error);
  }

  return res.send({
    input,
    output
  });
});

module.exports = router;

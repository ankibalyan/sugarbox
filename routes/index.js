var express = require('express');
const { getMinPositiveNum } = require('../utils');
var router = express.Router();

router.get('/', function(req, res, next) {
  return res.send({ status: 'ok' })
});

module.exports = router;

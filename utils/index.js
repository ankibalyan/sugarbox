function getMinPositiveNum(n) {
  const num = Number(n);

  for (let i = 1; i <= 100; i++) {
    if (i % num === 0 && i != num) {
      // check on sum of digits of divisible number
      const digitsSum = sumOfDigits(i);
      if (digitsSum === num) {
        return i;
      }
    }
  }
  return -1;
}


function sumOfDigits(n) {
  const arrDigits = String(n).split('');
  const sum = arrDigits.reduce((sum, curr) => sum + Number(curr), 0);
  return sum;
}


module.exports = {
  getMinPositiveNum,
  sumOfDigits
};